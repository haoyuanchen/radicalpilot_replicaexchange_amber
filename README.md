# RadicalPilot_ReplicaExchange_Amber #

## How do I get set up? ##

* The entire repo will be put under ReplicaExchange/src/radical/repex.
* At least you need to have sander.MPI executable which can be obtained from parallel compilation of the free AmberTools14 package. You don't have to have pmemd.MPI, which can only be obtained from Amber14 (not free).

## What are all these files for? ##

* activate; localrun_amber: shortcuts for activating virtualenv and running replica exchange in Amber locally, respectively.
* launch_simulation_amber.py: controlling script that runs the repex job.
* amber_inp/: contains Amber input templates
* kernels/: kernel configuration file. Amber specifications have been added into "localhost.linux.x86.64" item in kernels.py.
* config/: configuration info. Amber specifications added into config.info and replaced NAMD stuff in input.json.
* replicas/: added class Replica_Amber in replica.py.
* amber_kernels/: amber_kernel.py defines the AmberKernelS2 class. matrix_calculator_s2.py is modified to be able to grab data from Amber mdinfo file.

## TODO ##

* Run some real tests.
